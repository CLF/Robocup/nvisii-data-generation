download hdri maps from https://polyhaven.com/hdris 

## HDR Images used for Datasets on cerium:/vol/media 

 cayley_interior_4k.hdr             fireplace_4k.hdr           'quattro_canti_4k (1).hdr'          studio_garden_4k.hdr
 christmas_photo_studio_01_4k.hdr   gear_store_4k.hdr           quattro_canti_4k.hdr               studio_small_08_4k.hdr
 clarens_night_02_4k.hdr            lythwood_room_4k.hdr        rural_graffiti_tower_4k.hdr        teatro_massimo_2k.hdr
 colorful_studio_4k.hdr             modern_buildings_2_4k.hdr   sepulchral_chapel_rotunda_4k.hdr
 dark_autumn_forest_4k.hdr          phalzer_forest_01_4k.hdr    solitude_interior_4k.hdr
 download.md                        phone_shop_4k.hdr           studio_country_hall_4k.hdr