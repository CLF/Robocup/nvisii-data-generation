#!/usr/bin/env python3
import random 
import subprocess


# 50*500 images

scenes = 50
output = "/media/local/dope/training/gazebo_ycb"


for i in range(0, scenes):
	print(f"creating dataset {i} of {scenes}")
	to_call = [
		"python",'../random_gazebo_ycb.py',
		'--spp','4000',
		'--nb_frames', '500',
		'--objs_folder_distrators', 'google_scanned_models/',
		'--nb_distractors', str(int(random.uniform(2,6))),
		'--objs_folder', '/home/testuser/ws_dope/src/gazebo_ycb/models/',
		'--nb_objects', str(int(random.uniform(15,35))),
		'--noise', #optiplex denoise
		'--scale', '1',
		'--outf',f"{output}/{str(i).zfill(3)}",
		'--width', '640',
		'--height', '640',
	]
	subprocess.call(to_call)
